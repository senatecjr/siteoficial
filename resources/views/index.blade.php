<!DOCTYPE HTML>

<html>

	<head>

		<title>Senatec.Jr</title>

		<meta charset="utf-8" />

		<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no" />

		<meta name="description" content="" />

		<meta name="keywords" content="" />

		<link rel="stylesheet" href="{{ asset('css/main.css') }}" />

	</head>

	<body class="is-preload">



		<!-- Header -->

			<header id="header">

				<a class="logo" href="/">SENATEC JR</a>

				<nav>

					<a href="#menu"></a>

				</nav>

			</header>



		<!-- Nav -->

			<nav id="menu">

				<ul class="links">

   					<li><a href="register"><span class="glyphicon glyphicon-log-in"></span> Registrar-se</a></li>

   					<li><a href="login"><span class="glyphicon glyphicon-log-in"></span> Login</a></li>

				</ul>

			</nav>

			
		<!-- Banner -->

			<section id="banner">

				<div class="inner">

					<h1>Senatec</h1>

					<p>Empresa Jr, Adotando novas ideias que vão modificar o mercado de Pelotas.<br />

					Criando soluções e Inovando a cada dia.</p>

				</div>

				<video autoplay loop muted playsinline src="{{ asset('images/banner.mp4') }}"></video>
				
			</section>



		<!-- Highlights -->

			<section class="wrapper">

				<div class="inner">

						
					<header class="special">

							<h2>Quem Somos</h2>

					</header>

					<header class="specialj">
						<!-- mais uma gambiarra das boas usando style dentro do elemento --->
							<p style="width: 70%; margin-left: 15%;"> A Senatec foi fundada em julho de 2018 Incubada na Faculdade de Tecnologia Senac Pelotas.
								Trata-se de uma Empresa Jr. que tem como principal Objetivo Proporcionar a seus membros as condições 
								necessárias à aplicação prática de seus conhecimentos teóricos relativos à sua área de formação profissional.
								Nossas atividades são exercidas pelos cursos de Análise e Desenvolvimento de Sistemas, Marketing, Processos Gerenciais
								e Redes de computadores. Em nossos projetos visamos buscar o melhor de cada membro, para além de atingir os objetivos
								institucionais, proporcionar aos nossos clientes serviços que atendam às suas necessidades em todo processo de relacionamento com a Senatec. </p>

					</header>
					

				</div>

			</section>

			<section class="wrapper">

					<div class="inner">
	
						<header class="special">
	
							<h2>Projetos</h2>
	
						</header>
						
						<div class="highlights">

								<section>
		
									<div class="content">
		
										<header>
		
											<img src="{{ asset('images/bombeiro-logo2.png') }}" alt="" />
		
										</header>
		
										<p> Fornecer os serviços básicos ao Corpo de Bombeiros Militar, referente à
											rede de computadores, como cabeamento estruturado, segurança, servidor de arquivos
											compartilhado, sistema de backup e controle de acesso e Internet de alta velocidade.</p>
		
									</div>
		
								</section>
		
								<section>
		
									<div class="content">
		
										<header>
		
											<img src="{{ asset('images/infobyte-logo.png') }}" alt="" />
		
										</header>
		
										<p> Desenvolvimento do novo Site. Recebemos a tarefa de criar um novo Designing 
											com novas Funcionalidades para o site da Empresa. Nosso Objetivo é trazer mais 
											visibilidade ao conteúdo, e os serviços que a empresa fornece a seu consumidor.</p>
		
									</div>
		
								</section>
		
								<section>
		
									<div class="content">
		
										<header>
		
											<img src="{{ asset('images/senatec-logo.png') }}" alt="" />
		
										</header>
		
										<p> A união dos cursos do Senac de Técnologia e Gestão desenvolvem em conjunto todo o conteúdo do website
											e com a orientação dos professores construimos esse ambiente de Tecnologia da informação.</p>
		
									</div>
		
								</section>
		
							</div>
	
					</div>
	
				</section>



		<!-- CTA 

			<section id="cta" class="wrapper">

				<div class="inner">

					<h2>TITULOTITULOTITULO</h2>

					<p>textotextotextotex totextotextotexto textotextotextotexto textotextotextotextotextotextotexto textotextotexto 

textotextotex totextotextotexto textotextotextot extotextotextotextotextotexto textotextotextot extotextotextote xtotextotextotex totextotextotextotexto

textotextotex totextotextotex totextotextotextotexto textotextotextotextotexto</p>

				</div>

			</section>-->



		<!-- Diretores / Orientadores -->

			<section class="wrapper">

				<div class="inner">

					<header class="special">

						<h2>Diretores / Orietadores.</h2>

						<p>Atualmente a empresa jr esta em desenvolvimento.</p>

					</header>

					<div class="testimonials">

						<section>

								<div class="content">

									<div class="author">

										<div class="image">
		
											<img src="{{ asset('images/pic001.jpg') }}" alt="" />
		
										</div>
		
										<p class="credit">- <strong>Caroline Takahashi   </strong><span>Diretor(a) - Projetos.</span></p>
									
									</div>

									<p></p>
									<p>Cursando o 4º semestre de Processo Gerencias.</p>

								</div>

						</section>

						<section>

								<div class="content">

									<div class="author">

										<div class="image">
		
											<img src="{{ asset('images/pic002.jpg') }}" alt="" />
		
										</div>

										<p class="credit">- <strong>Fabricio Reyes Mota </strong><span> Diretor(a) - Presidente.</span></p>
									
									</div>

									<p></p>
									<p>Cursando o 4º semestre de ADS.</p>

								</div>

						</section>
						
						<section>

								<div class="content">

									<div class="author">

										<div class="image">
		
											<img src="{{ asset('images/pic003.jpg') }}" alt="" />
		
										</div>
		
										<p class="credit">- <strong>Gabriel da Cruz Ferreira  </strong><span>Diretor(a) - Recursos Humanos.</span></p>
									
									</div>

									<p></p>
									<p>Cursando o 2º semestre de Processo Gerencias.</p>

								</div>

						</section>
						
						<section>

								<div class="content">

									<div class="author">

										<div class="image">
		
											<img src="{{ asset('images/pic004.jpg') }}" alt="" />
		
										</div>
		
										<p class="credit">- <strong>Juliano Petit Vargas  </strong><span>Diretor(a) - Financeiro.</span></p>
									
									</div>

									<p></p>
									<p>Cursando o 4º semestre de Processo Gerencias.</p>

								</div>

						</section>
						
						<section>

								<div class="content">

									<div class="author">

										<div class="image">
		
											<img src="{{ asset('images/pic005.jpg') }}" alt="" />
		
										</div>
		
										<p class="credit">- <strong>Louise Duarte Siqueira  </strong><span>Diretor(a) - Redes.</span></p>
									
									</div>

									<p></p>
									<p>Cursando o 4º semestre de Redes.</p>

								</div>

						</section>
						
						<section>

								<div class="content">

									<div class="author">

										<div class="image">
		
											<img src="{{ asset('images/pic006.jpg') }}" alt="" />
		
										</div>
		
										<p class="credit">- <strong>Manuel Francisco Ferro  </strong><span>Diretor(a) - Marketing.</span></p>
									
									</div>

									<p></p>
									<p>Cursando o 2º semestre de Marketing.</p>

								</div>

						</section>
						
						<section>

								<div class="content">

									<div class="author">

										<div class="image">
		
											<img src="{{ asset('images/pic007.jpg') }}" alt="" />
		
										</div>
		
										<p class="credit">- <strong>Salomão Beling  </strong><span>Diretor(a) - ADS.</span></p>
									
									</div>

									<p></p>
									<p>Cursando o 4º semestre de ADS.</p>

								</div>

						</section>
						
						<section>

								<div class="content">

									<div class="author">

										<div class="image">
		
											<img src="{{ asset('images/pic008.jpg') }}" alt="" />
		
										</div>
		
										<p class="credit">- <strong>Angelo da Luz  </strong><span>Orientador(a) - ADS.</span></p>
									
									</div>

									<p></p>
									<p>Orientador do Curso de Analise e Desenvolvimento de Sistemas</p>

								</div>

						</section>

						<section>

								<div class="content">

									<div class="author">

										<div class="image">
		
											<img src="{{ asset('images/pic009.jpg') }}" alt="" />
		
										</div>
		
										<p class="credit">- <strong>Sergio Decker  </strong><span>Orientador(a) - Gestão.</span></p>
									
									</div>

									<p></p>
									<p>Orientador do Curso de Gestão</p>

								</div>

						</section>

					</div>

				</div>

			</section>



		<!-- Footer -->

			<footer id="footer">

				<div class="inner">

					<div class="content">

						<section>

							<h3>Endereço:</h3>

							<p> Rua Gonçalves Chaves, 602 -
								Centro -
								Pelotas -
								RS -
								96015-560</p>

								<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3392.0646395174535!2d-52.3406493845058!3d-31.768722881288046!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x644b62f2bc9bddd5!2sFaculdade+de+Tecnologia+Senac+Pelotas!5e0!3m2!1spt-BR!2sbr!4v1537138607721" width="90%" height="450" frameborder="-1" style="border: -1" allowfullscreen></iframe>
							<p></p>

						</section>

						<!-- div ficticia de alinhamento  -->


							<div class="ficticia">
								<br>
							</div>

						<!-- div ficticia criada por salomao beling hahahahaha eu posso  -->


					<section>
							<h3>Patrocinadores:</h3>
						<div class="patrocinio">
							<ul class="alt">


								<img class="ali1" width="150px" src="{{ asset('images/senac-logo.png') }}" alt="" />
								<img class="separa">
								<img class="ali2 hr" width="150px" src="{{ asset('images/fecomercio-logo.png') }}" alt="" />


							</ul>
						</div>
					</section>

						<!--<section>

							<h4>Patrocinadores</h4>

							<ul class="alt">

								<li><a href="#">Faculdade Senac</a></li>

								<li><a href="#">Fecomercio</a></li>

								<li><a href="#">sitesitesite.</a></li>

								<li><a href="#">sitesitesite.</a></li>

							</ul>

						</section>

						<section>

							<h4>Nos Encontre</h4>

							<ul class="plain">

								<li><a href="#Twitter"><i class="icon fa-twitter">&nbsp;</i>Twitter</a></li>

								<li><a href="#Facebook"><i class="icon fa-facebook">&nbsp;</i>Facebook</a></li>

								<li><a href="#Instagram"><i class="icon fa-instagram">&nbsp;</i>Instagram</a></li>

								<li><a href="#GitLab"><i class="icon fa-gitlab">&nbsp;</i>GitLab</a></li>

							</ul>

						</section>-->

					</div>

					<div class="copyright">

						&copy;2018. Senatec. Jr, Gerenciando seu estilo.

					</div>

				</div>

			</footer>



		<!-- Scripts -->

			<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

			<script src="https://cdnjs.cloudflare.com/ajax/libs/babel-core/6.1.19/browser.min.js"></script>

			<script src="{{ asset('js/breakpoints.min.js') }}"></script>

			<script src="{{ asset('js/util.js') }}"></script>

			<script src="{{ asset('js/main.js') }}"></script>

			



	</body>

</html>

